# Advent Of Code 2022

Lancer tous les exercices pour tous les profils :

```./mvnw clean compile && ./mvnw exec:java -q```

Lancer tous les exercices pour Max DS :

```./mvnw clean compile && ./mvnw exec:java -q -Dexec.args="MAXDS"```

Lancer tous les exercices pour DM :

```./mvnw clean compile && ./mvnw exec:java -q -Dexec.args="DM"```