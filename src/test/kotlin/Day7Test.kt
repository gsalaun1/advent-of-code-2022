import assertk.assertAll
import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import assertk.assertions.isInstanceOf
import assertk.assertions.isTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.io.PrintStream
import java.io.PrintWriter

class Day7Test {

    private lateinit var data: List<String>
    private lateinit var day7: Day7

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day7.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day7 = Day7()
    }

    @Test
    fun `Should sum size of directories lighter than 100000`() {
        assertThat(day7.runPart1(data)).isEqualTo(95437)
    }

    @Test
    fun `Should sum size of directories lighter than 100000 for second example file`() {
        data = object {}.javaClass.getResourceAsStream("day7-2.txt")?.bufferedReader()?.readLines() ?: emptyList()
        assertThat(day7.runPart1(data)).isEqualTo(95437)
    }

    @Test
    fun `Should find size of smallest directory to delete`() {
        assertThat(day7.runPart2(data)).isEqualTo(24933642)
    }

    @Test
    fun `Should return empty file system`() {
        val elvesRootDirectory = parseCommandsAndOutputs(emptyList())
        assertThat(elvesRootDirectory.isEmpty()).isTrue()
    }

    @Test
    fun `Should return empty file a system given a command to return to root`() {
        val elvesRootDirectory = parseCommandsAndOutputs(listOf("$ cd /"))
        assertThat(elvesRootDirectory.isEmpty()).isTrue()
    }

    @Test
    fun `Should return file system with files only at root`() {
        val commandsAndOutput = listOf(
            "$ cd /",
            "$ ls",
            "14848514 b.txt",
            "8504156 c.dat"
        )
        val elvesRootDirectory = parseCommandsAndOutputs(commandsAndOutput)
        assertThat(elvesRootDirectory.getContent()).containsExactly(
            ElvesFile("b.txt", 14848514),
            ElvesFile("c.dat", 8504156)
        )
    }

    @Test
    fun `Should return file system with files and an empty directory`() {
        val commandsAndOutput = listOf(
            "$ cd /",
            "$ ls",
            "dir a",
            "14848514 b.txt",
            "8504156 c.dat",
            "$ cd a",
            "$ ls"
        )
        val elvesRootDirectory = parseCommandsAndOutputs(commandsAndOutput)
        assertThat(elvesRootDirectory.getContent()).containsExactly(
            ElvesDirectory("a"),
            ElvesFile("b.txt", 14848514),
            ElvesFile("c.dat", 8504156)
        )
    }

    @Test
    fun `Should return file system with files and a directory with files`() {
        val commandsAndOutput = listOf(
            "$ cd /",
            "$ ls",
            "dir a",
            "14848514 b.txt",
            "8504156 c.dat",
            "$ cd a",
            "$ ls",
            "29116 f",
            "2557 g",
            "62596 h.lst"
        )
        val elvesRootDirectory = parseCommandsAndOutputs(commandsAndOutput)
        assertThat(elvesRootDirectory.getContent()).containsExactly(
            ElvesDirectory("a", listOf(ElvesFile("f", 29116), ElvesFile("g", 2557), ElvesFile("h.lst", 62596))),
            ElvesFile("b.txt", 14848514),
            ElvesFile("c.dat", 8504156)
        )
    }

    @Test
    fun `Should return file system as expressed in example file`() {
        val elvesRootDirectory = parseCommandsAndOutputs(data)
        assertThat(elvesRootDirectory.getContent()).containsExactly(
            ElvesDirectory(
                "a",
                listOf(
                    ElvesDirectory("e", listOf(ElvesFile("i", 584))),
                    ElvesFile("f", 29116),
                    ElvesFile("g", 2557),
                    ElvesFile("h.lst", 62596)
                )
            ),
            ElvesFile("b.txt", 14848514),
            ElvesFile("c.dat", 8504156),
            ElvesDirectory(
                "d",
                listOf(
                    ElvesFile("j", 4060174),
                    ElvesFile("d.log", 8033020),
                    ElvesFile("d.ext", 5626152),
                    ElvesFile("k", 7214296)
                )
            ),
        )
    }

    @Test
    fun `Should return all directories size`() {
        val elvesRootDirectory = parseCommandsAndOutputs(data)
        val directoriesSize = elvesRootDirectory.getDirectoriesSize()
        assertThat(directoriesSize).containsExactly(
            Pair("e", 584),
            Pair("a", 94853),
            Pair("d", 24933642),
            Pair("/", 48381165)
        )
    }

    @Test
    fun `Should return all directories size for second example file`() {
        data = object {}.javaClass.getResourceAsStream("day7-2.txt")?.bufferedReader()?.readLines() ?: emptyList()
        val elvesRootDirectory = parseCommandsAndOutputs(data)
        val directoriesSize = elvesRootDirectory.getDirectoriesSize()
        assertThat(directoriesSize).containsExactly(
            Pair("a", 584),
            Pair("a", 94853),
            Pair("d", 24933642),
            Pair("/", 48381165)
        )
    }

    @Nested
    inner class PrintIn {

        private lateinit var buffer: OutputStream
        private lateinit var stream: PrintStream

        @BeforeEach
        fun setup() {
            buffer = ByteArrayOutputStream()
            stream = PrintStream(buffer)
        }

        @Test
        fun `Should print empty file system`() {
            val elvesRootDirectory = parseCommandsAndOutputs(listOf("$ cd /"))
            elvesRootDirectory.printIn(stream)
            val printedDirectory = buffer.toString()
            assertThat(printedDirectory).isEqualTo(
                """
                - / (dir)
            """.trimIndent() + "\n"
            )
        }

        @Test
        fun `Should print file system with files only at root`() {
            val commandsAndOutput = listOf(
                "$ cd /",
                "$ ls",
                "14848514 b.txt",
                "8504156 c.dat"
            )
            val elvesRootDirectory = parseCommandsAndOutputs(commandsAndOutput)
            elvesRootDirectory.printIn(stream)
            val printedDirectory = buffer.toString()
            assertThat(printedDirectory).isEqualTo(
                """
                - / (dir)
                  - b.txt (file, size=14848514)
                  - c.dat (file, size=8504156)
            """.trimIndent() + "\n"
            )
        }

        @Test
        fun `Should print file system as expressed in example file`() {
            val elvesRootDirectory = parseCommandsAndOutputs(data)
            elvesRootDirectory.printIn(stream)
            val printedDirectory = buffer.toString()
            assertThat(printedDirectory).isEqualTo(
                """
            - / (dir)
              - a (dir)
                - e (dir)
                  - i (file, size=584)
                - f (file, size=29116)
                - g (file, size=2557)
                - h.lst (file, size=62596)
              - b.txt (file, size=14848514)
              - c.dat (file, size=8504156)
              - d (dir)
                - j (file, size=4060174)
                - d.log (file, size=8033020)
                - d.ext (file, size=5626152)
                - k (file, size=7214296)
        """.trimIndent() + "\n"
            )
        }

        @Test
        fun `Should print file system as expressed in second example file`() {
            data = object {}.javaClass.getResourceAsStream("day7-2.txt")?.bufferedReader()?.readLines() ?: emptyList()
            val elvesRootDirectory = parseCommandsAndOutputs(data)
            elvesRootDirectory.printIn(stream)
            val printedDirectory = buffer.toString()
            assertThat(printedDirectory).isEqualTo(
                """
            - / (dir)
              - a (dir)
                - a (dir)
                  - i (file, size=584)
                - f (file, size=29116)
                - g (file, size=2557)
                - h.lst (file, size=62596)
              - b.txt (file, size=14848514)
              - c.dat (file, size=8504156)
              - d (dir)
                - j (file, size=4060174)
                - d.log (file, size=8033020)
                - d.ext (file, size=5626152)
                - k (file, size=7214296)
        """.trimIndent() + "\n"
            )
        }
    }
}