import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class Day2Test {

    private lateinit var data: List<String>
    private lateinit var day2: Day2

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day2.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day2 = Day2()
    }

    @Test
    fun `Should count regular score for no rounds`() {
        assertThat(day2.runPart1(listOf(""))).isEqualTo(0)
    }

    @Test
    fun `Should count regular score for a round`() {
        assertThat(day2.runPart1(listOf("A Y"))).isEqualTo(8)
    }

    @Test
    fun `Should count regular score for data file`() {
        assertThat(day2.runPart1(data)).isEqualTo(15)
    }

    @Test
    fun `Should count strategy score for data file`() {
        assertThat(day2.runPart2(data)).isEqualTo(12)
    }
}

class RockPaperScissorRegularRoundTest {
    @Test
    fun `Should count score for winning against Rock`() {
        val round = Day2.RockPaperScissorRegularRound("A Y")
        assertThat(round.getScore()).isEqualTo(8)
    }

    @Test
    fun `Should count score for drawing against Rock`() {
        val round = Day2.RockPaperScissorRegularRound("B Y")
        assertThat(round.getScore()).isEqualTo(5)
    }

    @ParameterizedTest
    @MethodSource("provideAllPossibleRegularRounds")
    fun `Should compute all possible scores`(round: String, expectedScore: Int) {
        assertThat(Day2.RockPaperScissorRegularRound(round).getScore()).isEqualTo(expectedScore)
    }

    companion object {
        @JvmStatic
        fun provideAllPossibleRegularRounds() =
            Stream.of(
                Arguments.of("A X", 3 + 1),
                Arguments.of("A Y", 6 + 2),
                Arguments.of("A Z", 0 + 3),
                Arguments.of("B X", 0 + 1),
                Arguments.of("B Y", 3 + 2),
                Arguments.of("B Z", 6 + 3),
                Arguments.of("C X", 6 + 1),
                Arguments.of("C Y", 0 + 2),
                Arguments.of("C Z", 3 + 3)
            )
    }
}

class RockPaperScissorStrategyTest {

    @Test
    fun `Should count score for drawing against Rock`() {
        val round = Day2.RockPaperScissorStrategyRound("A Y")
        assertThat(round.getScore()).isEqualTo(4)
    }

    @Test
    fun `Should count score for losing against Paper`() {
        val round = Day2.RockPaperScissorStrategyRound("B X")
        assertThat(round.getScore()).isEqualTo(1)
    }

    @ParameterizedTest
    @MethodSource("provideAllPossibleStrategyRounds")
    fun `Should compute all possible scores`(round: String, expectedScore: Int) {
        assertThat(Day2.RockPaperScissorStrategyRound(round).getScore()).isEqualTo(expectedScore)
    }

    companion object {
        @JvmStatic
        fun provideAllPossibleStrategyRounds() =
            Stream.of(
                Arguments.of("A X", 0 + 3),
                Arguments.of("A Y", 3 + 1),
                Arguments.of("A Z", 6 + 2),
                Arguments.of("B X", 0 + 1),
                Arguments.of("B Y", 3 + 2),
                Arguments.of("B Z", 6 + 3),
                Arguments.of("C X", 0 + 2),
                Arguments.of("C Y", 3 + 3),
                Arguments.of("C Z", 6 + 1)
            )
    }
}
