import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class Day8Test {

    private lateinit var data: List<List<Int>>
    private lateinit var day8: Day8

    @BeforeEach
    fun setup() {
        day8 = Day8()
        data = day8.readData(object {}.javaClass.getResourceAsStream("day8.txt"))
    }

    @Test
    fun `Should count number of visible trees from outside`() {
        assertThat(day8.runPart1(data)).isEqualTo(21)
    }

    @Test
    fun `Should return highest scenic score`() {
        assertThat(day8.runPart2(data)).isEqualTo(8)
    }

    @Nested
    inner class ListTakeUntil {
        @Test
        fun `Should return all list if all list is lower than reference value`() {
            val list = listOf(1, 2, 3, 4)
            assertThat(list.takeUntil(5)).containsExactly(1, 2, 3, 4)
        }

        @Test
        fun `Should return list before taller value`() {
            val list = listOf(1, 2, 3, 5, 6)
            assertThat(list.takeUntil(4)).containsExactly(1, 2, 3, 5)
        }

        @Test
        fun `Should return list including first equal value, ignoring the following values`() {
            val list = listOf(1, 2, 3, 4, 4, 3, 6)
            assertThat(list.takeUntil(4)).containsExactly(1, 2, 3, 4)
        }

        @Test
        fun `Should return list including first taller value, ignoring the following values`() {
            val list = listOf(1, 2, 3, 5, 4, 3, 6)
            assertThat(list.takeUntil(4)).containsExactly(1, 2, 3, 5)
        }
    }
}