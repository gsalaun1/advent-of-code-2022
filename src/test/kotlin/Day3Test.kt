import assertk.assertThat
import assertk.assertions.containsOnly
import assertk.assertions.isEqualTo
import assertk.assertions.isFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day3Test {

    private lateinit var data: List<String>
    private lateinit var day3: Day3

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day3.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day3 = Day3()
    }

    @Test
    fun `Should compute sum of prioritised items`() {
        val itemsSum = day3.runPart1(data)
        assertThat(itemsSum).isEqualTo(157)
    }

    @Test
    fun `Should compute sum of badges`() {
        val itemsSum = day3.runPart2(data)
        assertThat(itemsSum).isEqualTo(70)
    }

    @Test
    fun `Should build rucksack`() {
        val ruckSack = buildRuckSack("vJrwpWtwJgWrhcsFMMfFFhFp")
        assertThat(ruckSack).isEqualTo(RuckSack("vJrwpWtwJgWr", "hcsFMMfFFhFp"))
    }

    @Test
    fun `Should find priority of common item in compartiments`() {
        val ruckSack = buildRuckSack("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")
        assertThat(ruckSack.getPriorityOfCommonItem()).isEqualTo(38)
    }

    @Test
    fun `Should find common item in three rucksacks`() {
        val ruckSack1 = buildRuckSack("vJrwpWtwJgWrhcsFMMfFFhFp")
        val ruckSack2 = buildRuckSack("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")
        val ruckSack3 = buildRuckSack("PmmdzqPrVvPwwTWBwg")
        val commonItem = ruckSack1.intersect(ruckSack2, ruckSack3)
        assertThat(commonItem).isEqualTo('r')
    }
}