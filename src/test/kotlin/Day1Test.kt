import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day1Test {

    private lateinit var data: List<String>
    private lateinit var day1: Day1

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day1.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day1 = Day1()
    }

    @Test
    fun `Should count max calories`() {
        assertThat(day1.runPart1(data)).isEqualTo(24000)
    }

    @Test
    fun `Should sum calories for top three elves`() {
        assertThat(day1.runPart2(data)).isEqualTo(45000)
    }
}
