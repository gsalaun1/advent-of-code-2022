import assertk.assertThat
import assertk.assertions.containsExactly
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day5Test {

    private lateinit var data: List<String>
    private lateinit var day5: Day5

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day5.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day5 = Day5()
    }

    @Test
    fun `Should compute message after crates movment`() {
        assertThat(day5.runPart1(data)).isEqualTo("CMZ")
    }

    @Test
    fun `Should compute message after crates movment with CrateMover 9001`() {
        assertThat(day5.runPart2(data)).isEqualTo("MCD")
    }

    @Test
    fun `Should init stacks with one stack of one crate`() {
        val stacks = Stacks(
            """
            [Z]
             1
        """.trimIndent()
        )
        assertThat(stacks.content).containsExactly(Stack(1, listOf('Z')))
    }

    @Test
    fun `Should init stacks with one stack of two crates`() {
        val stacks = Stacks(
            """
            [N]
            [Z]
             1
        """.trimIndent()
        )
        assertThat(stacks.content).containsExactly(Stack(1, listOf('Z', 'N')))
    }

    @Test
    fun `Should init stacks with two stacks of two crates`() {
        val stacks = Stacks(
            """
            [N] [C]
            [Z] [M]
             1   2
        """.trimIndent()
        )
        assertThat(stacks.content).containsExactly(
            Stack(1, listOf('Z', 'N')),
            Stack(2, listOf('M', 'C')),
        )
    }

    @Test
    fun `Should init stacks with two stacks of different size`() {
        val stacks = Stacks(
            """
                [D]
            [N] [C]
            [Z] [M]
             1   2
        """.trimIndent()
        )
        assertThat(stacks.content).containsExactly(
            Stack(1, listOf('Z', 'N')),
            Stack(2, listOf('M', 'C', 'D')),
        )
    }

    @Test
    fun `Should init stacks with three stacks of different size`() {
        val stacks = Stacks(
            """
                [D]
            [N] [C]
            [Z] [M] [P]
             1   2   3
        """.trimIndent()
        )
        assertThat(stacks.content).containsExactly(
            Stack(1, listOf('Z', 'N')),
            Stack(2, listOf('M', 'C', 'D')),
            Stack(3, listOf('P')),
        )
    }

    @Test
    fun `Should apply a movement to stacks with CrateMover9000`() {
        val stacks = Stacks(
            """
                [D]
            [N] [C]
            [Z] [M] [P]
             1   2   3
        """.trimIndent()
        )
        val crane = CrateMover9000()
        crane.move("move 1 from 2 to 1", stacks)
        assertThat(stacks.content).containsExactly(
            Stack(1, listOf('Z', 'N', 'D')),
            Stack(2, listOf('M', 'C')),
            Stack(3, listOf('P')),
        )
    }

    @Test
    fun `Should move more than ten crates from one stack to another with CrateMover9000`() {
        val stacks = Stacks(
            """
                [C]
                [P]
                [D]
                [M]
                [S]
                [W]    
                [C]
            [G] [F]
            [T] [V]
            [V] [P]
            [M] [Z]
            [P] [D]
             1   2
        """.trimIndent()
        )
        val crane = CrateMover9000()
        crane.move("move 11 from 2 to 1", stacks)
        assertThat(stacks.content).containsExactly(
            Stack(1, listOf('P','M','V','T','G','C','P','D','M','S','W','C','F','V','P','Z')),
            Stack(2, listOf('D'))
        )
    }
}
