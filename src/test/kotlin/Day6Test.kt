import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class Day6Test {

    private lateinit var communicationDevice: CommunicationDevice

    @BeforeEach
    fun setup() {
        communicationDevice = CommunicationDevice()
    }

    @ParameterizedTest
    @MethodSource("provideAllFirstStartOfPacketMarkerEndingPositions")
    fun `should find characters after first start of packet marker`(signal: String, expectedPosition: Int) {
        assertThat(communicationDevice.findFirstStartOfPacketMarkerEndingPosition(signal)).isEqualTo(expectedPosition)
    }

    @ParameterizedTest
    @MethodSource("provideAllFirstStartOfMessageMarkerEndingPositions")
    fun `should find characters after first start of message marker`(signal: String, expectedPosition: Int) {
        assertThat(communicationDevice.findFirstStartOfMessageMarkerEndingPosition(signal)).isEqualTo(expectedPosition)
    }

    companion object {
        @JvmStatic
        fun provideAllFirstStartOfPacketMarkerEndingPositions() =
            Stream.of(
                Arguments.of("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
                Arguments.of("nppdvjthqldpwncqszvftbrmjlhg", 6),
                Arguments.of("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
                Arguments.of("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11)
            )

        @JvmStatic
        fun provideAllFirstStartOfMessageMarkerEndingPositions() =
            Stream.of(
                Arguments.of("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
                Arguments.of("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
                Arguments.of("nppdvjthqldpwncqszvftbrmjlhg", 23),
                Arguments.of("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
                Arguments.of("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26)
                )
    }
}