import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class Day4Test {

    private lateinit var data: List<String>
    private lateinit var day4: Day4

    @BeforeEach
    fun setup() {
        data = object {}.javaClass.getResourceAsStream("day4.txt")?.bufferedReader()?.readLines() ?: emptyList()
        day4 = Day4()
    }

    @Test
    fun `Should count number of fully overlapping pairs`() {
        val fullyOverlappingPairs = day4.runPart1(data)
        assertThat(fullyOverlappingPairs).isEqualTo(2)
    }

    @Test
    fun `Should count number of fully or partially overlapping pairs`() {
        val fullyOrPartiallyOverlappingPairs = day4.runPart2(data)
        assertThat(fullyOrPartiallyOverlappingPairs).isEqualTo(4)
    }

    @Test
    fun `Should extract pair if differents`() {
        val pair = extractPair("2-4,6-8")
        assertThat(pair).isEqualTo(Pair(listOf(2, 3, 4), listOf(6, 7, 8)))
    }

    @Test
    fun `Should extract pair if a range has one section`() {
        val pair = extractPair("6-6,4-6")
        assertThat(pair).isEqualTo(Pair(listOf(6), listOf(4, 5, 6)))
    }

    @Test
    fun `Should return true if pair is overlapping`() {
        val pair = extractPair("6-6,4-6")
        assertThat(pair.isFullyOverlapping()).isTrue()
    }
}
