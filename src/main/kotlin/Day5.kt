import java.io.InputStream
import java.lang.IllegalStateException

fun main() {
    val day5 = Day5()
    existingTags.forEach { tag ->
        day5.run("day5-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 5 Part 1 : ${it.first.part1}")
            println("[$tag] Day 5 Part 2 : ${it.first.part2}")
        }
    }
}

class Day5 : Day<String, String, String>() {

    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) = runWithCrane(CrateMover9000(), lines)

    override fun runPart2(lines: List<String>) = runWithCrane(CrateMover9001(), lines)

    fun runWithCrane(crane: Crane, lines: List<String>): String {
        val stacksDefinitionLines = lines.filter { it.isNotBlank() && !it.startsWith("move") }
        val stacks = Stacks(stacksDefinitionLines)
        val movement = lines.filter { it.isNotBlank() && it.startsWith("move") }
        movement.forEach {
            crane.move(it, stacks)
        }
        return stacks.getTopWord()
    }
}

class Stack(val id: Int, initialCrates: List<Char>) {

    private val crates: MutableList<Char>

    init {
        crates = initialCrates.toMutableList()
    }

    fun removeTop(): Char {
        val topCrate = crates.removeLast()
        return topCrate
    }

    fun add(crate: Char) {
        crates.add(crate)
    }

    fun top(): Char = crates.last()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Stack

        if (id != other.id) return false
        if (crates != other.crates) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + crates.hashCode()
        return result
    }

    override fun toString(): String {
        return "Stack(id=$id, crates=$crates)"
    }

    fun removeLast(cratesToRemove: Int): List<Char> {
        val lastChars = crates.takeLast(cratesToRemove)
        (1..cratesToRemove).forEach { _ -> crates.removeLast() }
        return lastChars
    }
}

class Stacks(values: List<String>) {

    constructor(value: String) : this(value.split("\n"))

    val content: List<Stack>

    init {
        val crateLines = mutableMapOf<Int, List<Char>>()
        (values - values.last()).forEach { line ->
            val lineCrates = extractCrates(line)
            lineCrates.forEach { crateLines[it.key] = (crateLines[it.key] ?: emptyList()) + it.value }
        }
        val crateIds = extractIds(values.last())
        content = crateLines.mapNotNull { crateLine ->
            crateIds[crateLine.key]?.let { stackId ->
                Stack(stackId, crateLine.value.filter { !it.isWhitespace() }.reversed())
            }
        }
    }

    private fun extractCrates(line: String): Map<Int, Char> {
        val nbCrates = (line.length + 1) / 4
        return (0 until nbCrates).map {
            it to line[it * 4 + 1]
        }.toMap()
    }

    private fun extractIds(line: String): Map<Int, Int> {
        val ids = line.split(" ").filter { it.isNotBlank() }
        return (ids.indices).map { it to ids[it].toInt() }.toMap()
    }

    override fun toString(): String {
        return "Stacks(content=$content)"
    }

    fun getTopWord(): String = content.map { it.top() }.joinToString("")

    fun getById(stackId: Int) =
        content.firstOrNull { it.id == stackId } ?: throw IllegalStateException("Stack $stackId not found !")
}

sealed interface Crane {
    fun move(movement: String, stacks: Stacks)
}

class CrateMover9000 : Crane {
    override fun move(movement: String, stacks: Stacks) {
        val criteria = movement.split(" ")
        val cratesToMove = criteria[1].toInt()
        val source = stacks.getById(criteria[3].toInt())
        val target = stacks.getById(criteria[5].toInt())
        (1..cratesToMove).forEach {
            val crate = source.removeTop()
            target.add(crate)
        }
    }
}

class CrateMover9001 : Crane {
    override fun move(movement: String, stacks: Stacks) {
        val criteria = movement.split(" ")
        val cratesToMove = criteria[1].toInt()
        val source = stacks.getById(criteria[3].toInt())
        val target = stacks.getById(criteria[5].toInt())
        val crates = source.removeLast(cratesToMove)
        crates.forEach { char ->
            target.add(char)
        }
    }
}