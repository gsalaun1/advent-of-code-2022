import java.io.InputStream

fun main() {
    val day1 = Day1()
    existingTags.forEach { tag ->
        day1.run("day1-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 1 Part 1 : ${it.first.part1}")
            println("[$tag] Day 1 Part 2 : ${it.first.part2}")
        }
    }
}

class Day1 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) = countCaloriesPerElf(lines).max()

    override fun runPart2(lines: List<String>) =
        countCaloriesPerElf(lines).sorted().takeLast(3).sum()

    private fun countCaloriesPerElf(lines: List<String>): List<Int> {
        val calories = Pair<MutableList<Int>, Int>(mutableListOf(), 0)
        return lines.fold(calories) { acc, current ->
            if (current == "") {
                Pair((acc.first + acc.second).toMutableList(), 0)
            } else {
                Pair(acc.first, acc.second + current.toInt())
            }
        }.let { Pair(it.first + it.second, 0) }.first
    }
}
