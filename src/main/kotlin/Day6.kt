import java.io.InputStream
import java.lang.IllegalStateException
import kotlin.math.sign

fun main() {
    val day6 = Day6()
    existingTags.forEach { tag ->
        day6.run("day6-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 6 Part 1 : ${it.first.part1}")
            println("[$tag] Day 6 Part 2 : ${it.first.part2}")
        }
    }
}

class Day6 : Day<String, Int, Int>() {

    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val communicationDevice = CommunicationDevice()
        return communicationDevice.findFirstStartOfPacketMarkerEndingPosition(lines.first())
    }

    override fun runPart2(lines: List<String>): Int {
        val communicationDevice = CommunicationDevice()
        return communicationDevice.findFirstStartOfMessageMarkerEndingPosition(lines.first())
    }
}

class CommunicationDevice {
    fun findFirstStartOfPacketMarkerEndingPosition(signal: String) =
        findFirstDifferentCharactersEndingPosition(signal, 4)

    fun findFirstStartOfMessageMarkerEndingPosition(signal: String) =
        findFirstDifferentCharactersEndingPosition(signal, 14)

    private fun findFirstDifferentCharactersEndingPosition(signal: String, rangeSize: Int): Int {
        var idx = 0
        while (idx < signal.length - rangeSize) {
            val rangeChars = (idx until (idx + rangeSize)).map { signal[it] }.toSet()
            if (rangeChars.size == rangeSize) {
                break
            }
            idx++
        }
        return idx + rangeSize
    }
}