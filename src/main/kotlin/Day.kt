import java.io.InputStream
import java.time.Duration
import java.time.temporal.ChronoUnit
import kotlin.system.measureNanoTime

abstract class Day<InputCollectionType, Part1OutputType, Part2OutputType> {
    fun run(fileName: String): Pair<Results, Durations>? {
        var dataLines: List<InputCollectionType>
        val readingFileDuration = measureNanoTime {
            val dataStream = object {}.javaClass.getResourceAsStream(fileName) ?: return null
            dataLines = readData(dataStream)
            dataStream.close()
        }
        var resultPart1: Part1OutputType
        val part1Duration = measureNanoTime {
            resultPart1 = runPart1(dataLines)
        }
        var resultPart2: Part2OutputType
        val part2Duration = measureNanoTime {
            resultPart2 = runPart2(dataLines)
        }
        return Pair(
            Results(resultPart1.toString(), resultPart2.toString()),
            Durations(
                Duration.of(readingFileDuration, ChronoUnit.NANOS).toFullSeconds(),
                Duration.of(part1Duration, ChronoUnit.NANOS).toFullSeconds(),
                Duration.of(part2Duration, ChronoUnit.NANOS).toFullSeconds()
            )
        )
    }

    abstract fun readData(data: InputStream): List<InputCollectionType>

    abstract fun runPart1(lines: List<InputCollectionType>): Part1OutputType

    abstract fun runPart2(lines: List<InputCollectionType>): Part2OutputType
}

fun Duration.toFullSeconds() = this.toString().replace("PT", "").replace("S", "").toDouble()
