import de.m3y.kformat.Table
import de.m3y.kformat.table
import java.util.Locale
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.functions
import kotlin.system.exitProcess

val existingTags = listOf("MAXDS", "DM")

fun main(vararg args: String) {
    val selectedTags = if (args.isEmpty()) {
        existingTags
    } else {
        existingTags.intersect(args.asIterable().toSet())
    }
    if (selectedTags.isEmpty()) {
        println("No existing Tag selected")
        exitProcess(1)
    }
    selectedTags.forEach { tag ->
        println("Advent Of Code for [$tag]\n")

        val metrics = (1..25).mapNotNull { day ->
            try {
                val currentDayClass = Class.forName("Day$day").kotlin
                val runMethod = currentDayClass.functions.firstOrNull { function -> function.name == "run" }
                if (runMethod != null) {
                    println("Collecting data for day $day")
                    val dayData = runMethod.call(
                        currentDayClass.createInstance(),
                        "day$day-${tag.lowercase(Locale.getDefault())}.txt"
                    ) as Pair<Results, Durations>?
                    if (dayData != null) {
                        day to dayData
                    } else {
                        null
                    }
                } else {
                    null
                }
            } catch (exception: ClassNotFoundException) {
                null
            }
        }.toMap()

        val totalTime = metrics.map { it.value.second.total() }.sum()

        val metricsTable = table {
            header(
                "Jour",
                "Part 1 - Résultat",
                "Part 2 - Résultat",
                "Lecture du fichier - Durée",
                "Part 1 - Durée",
                "Part 2 - Durée",
                "Durée totale"
            )

            metrics.forEach { (day, metric) ->
                row(
                    day.toString(),
                    metric.first.part1,
                    metric.first.part2,
                    metric.second.fileRead.formatAtBestUnit(),
                    metric.second.part1.formatAtBestUnit(),
                    metric.second.part2.formatAtBestUnit(),
                    metric.second.total().formatAtBestUnit()
                )
            }
            row(
                "Total",
                "---",
                "---",
                "---",
                "---",
                "---",
                totalTime.formatAtBestUnit()
            )
            hints {
                borderStyle = Table.BorderStyle.SINGLE_LINE
            }
        }.render(StringBuilder())

        println(metricsTable)
    }
}

data class Results(val part1: String, val part2: String)

data class Durations(
    val fileRead: Double,
    val part1: Double,
    val part2: Double
) {
    fun total() = part1 + part2 + fileRead
}

fun Double.formatAtBestUnit(): String {
    if (this < 1) {
        val timeInMs = this * 1000
        if (timeInMs < 1) {
            val timeInMicros = timeInMs * 1000
            return "${timeInMicros.round(3)} μs"
        }
        return "${timeInMs.round(3)} ms"
    }
    return "${this.round(3)} s"
}

fun Double.round(decimals: Int): String = "%.${decimals}f".format(this)
