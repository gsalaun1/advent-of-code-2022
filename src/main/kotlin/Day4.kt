import java.io.InputStream

fun main() {
    val day4 = Day4()
    existingTags.forEach { tag ->
        day4.run("day4-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 4 Part 1 : ${it.first.part1}")
            println("[$tag] Day 4 Part 2 : ${it.first.part2}")
        }
    }
}

class Day4 : Day<String, Int, Int>() {

    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines.map { extractPair(it) }.count { it.isFullyOverlapping() }

    override fun runPart2(lines: List<String>) =
        lines.map { extractPair(it) }.count { it.isFullyOrPartiallyOverlapping() }
}

fun extractPair(pair: String): Pair<List<Int>, List<Int>> {
    val splittedPair = pair.split(",")
    val firstElf = splittedPair[0].split("-").map { it.toInt() }
    val completeFirstElf = (firstElf[0]..firstElf[1]).toList()
    val secondElf = splittedPair[1].split("-").map { it.toInt() }
    val completeSecondElf = (secondElf[0]..secondElf[1]).toList()
    return Pair(completeFirstElf, completeSecondElf)
}

fun Pair<List<Int>, List<Int>>.isFullyOverlapping(): Boolean {
    return first.containsAll(second) || second.containsAll(first)
}

fun Pair<List<Int>, List<Int>>.isFullyOrPartiallyOverlapping(): Boolean {
    return isFullyOverlapping() || first.intersect(second).isNotEmpty()
}
