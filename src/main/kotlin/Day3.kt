import java.io.InputStream

fun main() {
    val day3 = Day3()
    existingTags.forEach { tag ->
        day3.run("day3-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 3 Part 1 : ${it.first.part1}")
            println("[$tag] Day 3 Part 2 : ${it.first.part2}")
        }
    }
}

class Day3 : Day<String, Int, Int>() {

    companion object {
        val lowerCaseItemsPriorities =
            ('a'..'z').mapIndexed { index, currentChar -> currentChar to (index + 1) }.toMap()
        val upperCaseItemsPriorities =
            ('A'..'Z').mapIndexed { index, currentChar -> currentChar to (index + 27) }.toMap()
        val ITEMSPRIORITIES = lowerCaseItemsPriorities + upperCaseItemsPriorities
    }

    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines.map { buildRuckSack(it) }.sumOf { it.getPriorityOfCommonItem() }

    override fun runPart2(lines: List<String>) =
        lines.map { buildRuckSack(it) }
            .chunked(3)
            .map { it[0].intersect(it[1], it[2]) }
            .mapNotNull { ITEMSPRIORITIES[it] }
            .sum()
}

fun buildRuckSack(items: String) = RuckSack(items.chunked(items.length / 2)[0], items.chunked(items.length / 2)[1])

data class RuckSack(val serializedFirstCompartment: String, val serializedSecondCompartment: String) {

    private val firstCompartment: List<Char>
    private val secondCompartment: List<Char>

    init {
        firstCompartment = serializedFirstCompartment.toList()
        secondCompartment = serializedSecondCompartment.toList()
    }

    fun getPriorityOfCommonItem(): Int {
        val commonItem = firstCompartment.intersect(secondCompartment).firstOrNull()
            ?: throw IllegalStateException("No common item in compartiments")
        return Day3.ITEMSPRIORITIES[commonItem] ?: throw IllegalStateException("Item not found in dictionary")
    }

    fun intersect(otherRuckSack1: RuckSack, otherRuckSack2: RuckSack): Char {
        val allCompartmentsCurrent = firstCompartment + secondCompartment
        val allCompartmentsOtherRuckSack1 = otherRuckSack1.firstCompartment + otherRuckSack1.secondCompartment
        val allCompartmentsOtherRuckSack2 = otherRuckSack2.firstCompartment + otherRuckSack2.secondCompartment
        return allCompartmentsCurrent
            .intersect(allCompartmentsOtherRuckSack1)
            .intersect(allCompartmentsOtherRuckSack2)
            .first()
    }
}
