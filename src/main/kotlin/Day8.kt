import java.io.InputStream
import java.io.PrintStream

fun main() {
    val day8 = Day8()
    existingTags.forEach { tag ->
        day8.run("day8-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 8 Part 1 : ${it.first.part1}")
            println("[$tag] Day 8 Part 2 : ${it.first.part2}")
        }
    }
}

class Day8 : Day<List<Int>, Int, Int>() {

    override fun readData(data: InputStream) =
        data.bufferedReader().readLines().map { line ->
            line.map { it.toString().toInt() }
        }

    override fun runPart1(lines: List<List<Int>>) =
        lines.mapIndexed { idxLine, line ->
            line.filterIndexed { idxTree, tree ->
                val topTrees = lines.topOf(idxLine, idxTree)
                val bottomTrees = lines.bottomOf(idxLine, idxTree)
                val leftTrees = lines.leftOf(idxLine, idxTree)
                val rightTrees = lines.rightOf(idxLine, idxTree)
                val visibleTop = topTrees.isEmpty() || topTrees.max() < tree
                val visibleBottom = bottomTrees.isEmpty() || bottomTrees.max() < tree
                val visibleLeft = leftTrees.isEmpty() || leftTrees.max() < tree
                val visibleRight = rightTrees.isEmpty() || rightTrees.max() < tree
                visibleTop || visibleBottom || visibleLeft || visibleRight
            }.count()
        }.sum()

    override fun runPart2(lines: List<List<Int>>) =
        lines.mapIndexed { idxLine, line ->
            line.mapIndexed { idxTree, tree ->
                val topTrees = lines.topOf(idxLine, idxTree).reversed()
                val bottomTrees = lines.bottomOf(idxLine, idxTree)
                val leftTrees = lines.leftOf(idxLine, idxTree).reversed()
                val rightTrees = lines.rightOf(idxLine, idxTree)
                val topScore = topTrees.takeUntil(tree).count()
                val bottomScore = bottomTrees.takeUntil(tree).count()
                val leftScore = leftTrees.takeUntil(tree).count()
                val rightScore = rightTrees.takeUntil(tree).count()
                topScore * bottomScore * leftScore * rightScore
            }.max()
        }.max()
}

fun List<List<Int>>.topOf(line: Int, column: Int): List<Int> =
    (0 until line).map { this[it][column] }

fun List<List<Int>>.bottomOf(line: Int, column: Int): List<Int> =
    (line + 1 until this.size).map { this[it][column] }

fun List<List<Int>>.leftOf(line: Int, column: Int): List<Int> =
    (0 until column).map { this[line][it] }

fun List<List<Int>>.rightOf(line: Int, column: Int): List<Int> =
    (column + 1 until this[line].size).map { this[line][it] }

fun List<Int>.takeUntil(value: Int): List<Int> {
    var valueFound = false
    return this.mapNotNull {
        if (valueFound) {
            null
        } else if (it < value) {
            it
        } else if (it > value) {
            valueFound = true
            it
        } else {
            valueFound = true
            it
        }
    }
}