import java.io.InputStream
import java.io.PrintStream

fun main() {
    val day7 = Day7()
    existingTags.forEach { tag ->
        day7.run("day7-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 7 Part 1 : ${it.first.part1}")
            println("[$tag] Day 7 Part 2 : ${it.first.part2}")
        }
    }
}

class Day7 : Day<String, Int, Int>() {

    companion object {
        const val TOTAL_DISK_SPACE = 70000000
        const val REQUIRED_UNUSED_SPACE = 30000000
    }

    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>): Int {
        val fileSystem = parseCommandsAndOutputs(lines)
        return fileSystem.getDirectoriesSize().map { it.second }.filter { it < 100000 }.sum()
    }

    override fun runPart2(lines: List<String>) : Int {
        val fileSystem = parseCommandsAndOutputs(lines)
        val occupiedDiskSpace = fileSystem.getSize()
        val minimalSpaceToFree = REQUIRED_UNUSED_SPACE - (TOTAL_DISK_SPACE - occupiedDiskSpace)
        return fileSystem.getDirectoriesSize().map { it.second }.filter { it >= minimalSpaceToFree }.min()
    }
}

fun parseCommandsAndOutputs(commandsAndOutput: List<String>): ElvesDirectory {
    val rootDirectory = ElvesDirectory("/")
    var currentDirectory: ElvesDirectory = rootDirectory
    commandsAndOutput
        .forEach { outputOrCommand ->
            if (outputOrCommand.startsWith("$")) {
                if (outputOrCommand.startsWith("$ cd")) {
                    val directoryName = outputOrCommand.split(" ")[2]
                    currentDirectory = when (directoryName) {
                        "/" -> rootDirectory
                        ".." -> currentDirectory.parent as ElvesDirectory
                        else -> currentDirectory.getContent().first { it.name == directoryName } as ElvesDirectory
                    }
                }
            } else {
                val outputProperties = outputOrCommand.split(" ")
                if (outputOrCommand.startsWith("dir")) {
                    val directory = ElvesDirectory(outputProperties[1])
                    directory.parent = currentDirectory
                    currentDirectory.add(directory)
                }
                if (outputOrCommand[0].isDigit()) {
                    val file = ElvesFile(outputProperties[1], outputProperties[0].toInt())
                    currentDirectory.add(file)
                }
            }
        }
    return rootDirectory
}

sealed class ElvesFileSystemComponent(open val name: String) {

    companion object {
        const val BASE_MARGIN = "  "
    }

    abstract fun getSize(): Int

    abstract fun printIn(stream: PrintStream, margin: String = "")
}

class ElvesDirectory(
    override val name: String,
    initialContent: List<ElvesFileSystemComponent> = emptyList(),
    var parent: ElvesDirectory? = null
) :
    ElvesFileSystemComponent(name) {

    private val content: MutableList<ElvesFileSystemComponent>

    init {
        content = initialContent.toMutableList()
    }

    fun isEmpty() = content.isEmpty()

    fun getContent() = content.toList()

    fun add(elvesFileSystemComponent: ElvesFileSystemComponent) = content.add(elvesFileSystemComponent)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ElvesDirectory

        if (name != other.name) return false
        if (content != other.content) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + content.hashCode()
        return result
    }

    fun getDirectoriesSize(): List<Pair<String, Int>> {
        val result = this.content.filterIsInstance<ElvesDirectory>()
            .fold(emptyList<Pair<String, Int>>()) { acc, item -> acc + item.getDirectoriesSize() }
        return result + Pair(this.name, this.getSize())
    }

    override fun getSize(): Int {
        return this.content.sumOf { it.getSize() }
    }

    override fun printIn(stream: PrintStream, margin: String) {
        stream.println("$margin- $name (dir)")
        content.forEachIndexed { idx, component ->
            component.printIn(stream, margin + BASE_MARGIN)
        }
    }
}

data class ElvesFile(override val name: String, private val size: Int) : ElvesFileSystemComponent(name) {
    override fun getSize() = size
    override fun printIn(stream: PrintStream, margin: String) {
        stream.println("$margin- $name (file, size=$size)")
    }
}