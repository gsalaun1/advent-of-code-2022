import java.io.InputStream

fun main() {
    val day2 = Day2()
    existingTags.forEach { tag ->
        day2.run("day2-${tag.lowercase()}.txt")?.let {
            println("[$tag] Day 2 Part 1 : ${it.first.part1}")
            println("[$tag] Day 2 Part 2 : ${it.first.part2}")
        }
    }
}

class Day2 : Day<String, Int, Int>() {
    override fun readData(data: InputStream) = data.bufferedReader().readLines()

    override fun runPart1(lines: List<String>) =
        lines.filter { it.isNotBlank() }.sumOf { RockPaperScissorRegularRound(it).getScore() }

    override fun runPart2(lines: List<String>) =
        lines.filter { it.isNotBlank() }.sumOf { RockPaperScissorStrategyRound(it).getScore() }

    enum class Shape(private val values: List<String>, val score: Int) {
        ROCK(listOf("A", "X"), 1),
        PAPER(listOf("B", "Y"), 2),
        SCISSORS(listOf("C", "Z"), 3);

        fun winsOver(opponent: Shape): Boolean {
            return opponent.getNemesis() == this
        }

        fun getNemesis() =
            when (this) {
                ROCK -> PAPER
                PAPER -> SCISSORS
                SCISSORS -> ROCK
            }

        fun isNemesisOf() =
            when (this) {
                ROCK -> SCISSORS
                PAPER -> ROCK
                SCISSORS -> PAPER
            }

        companion object {
            fun fromValue(value: String) = Shape.values().firstOrNull { it.values.contains(value) }
        }
    }

    abstract class RockPaperScissorRound(round: String) {
        val opponent: Shape
        val response: Shape

        init {
            val splittedRound = round.split(" ")
            opponent = Shape.fromValue(splittedRound[0])
                ?: throw IllegalStateException("Non existing shape : ${splittedRound[0]}")
            response = Shape.fromValue(splittedRound[1])
                ?: throw IllegalStateException("Non existing shape : ${splittedRound[1]}")
        }

        abstract fun getScore(): Int
    }

    class RockPaperScissorRegularRound(round: String) : RockPaperScissorRound(round) {

        override fun getScore(): Int {
            var result = 0
            result += response.score
            if (response == opponent) {
                result += 3
            }
            if (response.winsOver(opponent)) {
                result += 6
            }
            return result
        }
    }

    class RockPaperScissorStrategyRound(round: String) : RockPaperScissorRound(round) {

        override fun getScore() =
            when (response) {
                Shape.ROCK -> opponent.isNemesisOf().score
                Shape.PAPER -> opponent.score + 3
                Shape.SCISSORS -> opponent.getNemesis().score + 6
            }
    }
}
